<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?><?php print $styles ?>
<?php 
/**
* this part is returned for IE browsers only. 3 arguments are possible in the following order: path to ie6, ie7 and ie all
* this method is used mostly to clean up code a bit and do not return unnecessary merkup for decent browsers
**/
print phptemplate_css_iefix('ie6-fix.css','ie7-fix.css');

?>
<?php print $scripts ?>
<!--[if IE 6]>
<script src="/sites/all/themes/smoothBlue/js/DD_belatedPNG_0.0.7a.js"></script>
<script>
  /* EXAMPLE */
  DD_belatedPNG.fix('#block_content, #under_block, .block-title, #logo img, #search, #search-theme-form, #upheader #user-bar');
  
  /* string argument can be any CSS selector */
  /* .png_bg example is unnecessary */
  /* change it to what suits you! */
</script>
<![endif]--> 
</head>
<body class="<?php print $body_classes; ?>">
<?php print $header; ?>
<div id="wrapper">
  <div id="upheader">
    <?php print smoothBlue_user_bar()?>
    <?php print $search_box ?>
    <div class="clear"></div>
    <div id="secondary_links">
    <?php if (isset($secondary_links)) { ?>
      <?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'secondary')) ?>
    <?php } ?>
	</div>   
  </div>
  <div id="header">
	<?php if ($logo) { 
	  print l('<img src="'.$logo.'" alt="'.t('Home').'" />', '<front>', array('attributes' => array('id'=>'logo'), 'html'=>'TRUE'));
		}
	?>
	<?php if ($site_name) { ?>
      <h1 class="big_bold"><?php print $site_name ?></h1>
    <?php } ?>	
  </div>
  <div class="clear"></div>
  <div id="nav">
    <?php if (isset($primary_links)) { ?>
      <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'primary')) ?>
    <?php } ?>
  </div>    
  <div id="content">
    <div id="content_top">
      <div id="content_bottom">
        <div id="center" class="column">
          <?php if ($title) {  ?>
  	        <h1 class="title"><?php print $title ?></h1>
  		  <?php } ?>
          <?php if ($tabs) { ?>
  	        <div class="tabs">
              <?php print $tabs ?>
	        </div>
          <?php } ?>
          <?php print $breadcrumb ?>
          <?php print $help ?>
          <?php print $messages ?><?php print $content; ?>
        </div> 
        <div id="right" class="column">
          <?php  print $right;  ?>
        </div> 
        <div class="clear"></div>
      </div>
    </div>    
  </div>
  <div class="clear"></div>   
  <div id="footer">
    <div id="footer_top">
      <div id="footer_bottom">
        <?php print $footer; ?>
        <?php print smoothBlue_footer($footer_message); ?>
      </div>
    </div>   
  </div>
</div>
<?php print $closure ?>
</body>
</html>
